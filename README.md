# TiramiProt:
Contact prediction based on the Tiramisu (Fully Convolutional DenseNet) deep learning architecture.

## Installation instructions:

    pip3 install numpy Cython &&
    pip3 install -U git+https://github.com/serge-sans-paille/pythran@master &&
    pip3 install -U git+https://github.com/ElofssonLab/pyGaussDCA.git@master &&
    pip3 install .

You will also need a deep learning backend compatible with Keras. We recommend Tensorflow:

    pip3 install -U tensorflow

TiramiProt is written in PyTorch. Currently it requires PyTorch 0.3.1 to run:

    conda install pytorch=0.3.1 -c pytorch


## Usage instructions

Inside Python:

    import tiramiprot

    model = tiramiprot.get_tiramiprot()

    pred_1 = tiramiprot.predict(model, 'path/to/alignment1')
    pred_2 = tiramiprot.predict(model, 'path/to/alignment2')

## Attribution

This model was first published as a part of the master's thesis "Protein contact prediction based on the Tiramisu deep learning architecture", submitted at KTH, June 2018 by Nikos Tsardakas Renhuldt. It is the result of work done at the (Elofsson lab)[www.bioinfo.se], under the supervision of Arne Elofsson. This python package is based on code produced by Mirco Michel and David Menéndez Hurtado for the PconsC4 contact prediction model, available (here)[https://github.com/ElofssonLab/PconsC4].

Some of the code in this repository has been adapted from the Fully Convolutional DenseNet.
Original code may be found at
https://github.com/SimJeg/FC-DenseNet
S. Jégou, M. Drozdzal, D. Vazquez, A. Romero, and Y. Bengio, “The One Hundred Layers Tiramisu: Fully Convolutional DenseNets for Semantic Segmentation,” arXiv:1611.09326 [cs], Nov. 2016.

Code has also adapted from the PyTorch implementation of the Memory-Efficient DenseNet.
Original code may be found at
https://github.com/gpleiss/efficient_densenet_pytorch
G. Pleiss, D. Chen, G. Huang, T. Li, L. van der Maaten, and K. Q. Weinberger, “Memory-Efficient Implementation of DenseNets,” arXiv:1707.06990 [cs], Jul. 2017.

