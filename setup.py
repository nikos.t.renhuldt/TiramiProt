from __future__ import division
import os
from setuptools import setup, Extension, find_packages

from Cython.Build import cythonize
import numpy as np

# Check wheter we have pyx or c files:
base_path = os.path.dirname(os.path.abspath(__file__))

# A sdist will have C files, use those:
if os.path.exists(os.path.join(base_path, 'tiramiprot/parsing/_load_data.c')):
    use_cython = False
else:
    # It appears we are on git, go ahead and cythonice everything
    use_cython = True

flags = "-O2 -march=native -pipe -mtune=native".split()

if use_cython:
    extensions = [
        Extension(
            'tiramiprot.parsing._load_data', ['tiramiprot/parsing/_load_data.pyx'],
            include_dirs=[np.get_include()],
            extra_compile_args=flags,
            extra_link_args=flags),
        Extension(
            'tiramiprot.parsing._mi_info', ['tiramiprot/parsing/_mi_info.pyx'],
            include_dirs=[np.get_include()],
            extra_compile_args=flags,
            extra_link_args=flags)
    ]
else:
    extensions = [
        Extension(
            'tiramiprot.parsing._load_data', ['tiramiprot/parsing/_load_data.c'],
            include_dirs=[np.get_include()],
            extra_compile_args=flags,
            extra_link_args=flags),
        Extension(
            'tiramiprot.parsing._mi_info', ['tiramiprot/parsing/_mi_info.c'],
            include_dirs=[np.get_include()],
            extra_compile_args=flags,
            extra_link_args=flags)
    ]

setup(
    name='tiramiprot',
    version='0.1',
    description='',
    url='https://github.com/ElofssonLab/TiramiProt',
    author='Nikos Tsardakas Renhuldt and David Menéndez Hurtado and Mirco Michel',
    author_email='nikostrenhuldt@gmail.com',
    license='GPLv3',
    packages=find_packages(),
    include_dirs=[np.get_include()],
    package_data={
        'tiramiprot.models': [
            'tiramiprot/models/model_hyperparams.h5',
            'tiramiprot/models/model.t7',
            'tiramiprot/models/ss_pred_resnet_elu_nolr_dropout01_l26_large_v3_saved_model.h5'
        ]
    },
    include_package_data=True,
    ext_modules=cythonize(extensions),
    install_requires=open('requirements.txt').read().splitlines(),
    setup_requires=['numpy', 'Cython'],
    classifiers=[
        'Programming Language :: Python',
        'Topic :: Scientific/Engineering :: Bio-Informatics',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: '
        'GNU General Public License v3 (GPLv3)'
    ],
    zip_safe=False)

