from __future__ import division
import os
import h5py

import keras.backend as K
from keras.models import Model, load_model
from keras.layers import Input
from keras.layers.core import Lambda

import torch

from .Tiramisu import Network

def self_outer(x):
    outer_x = x[:, :, None, :] * x[:, None, :, :]
    return outer_x

def create_ss_model():
    """Load the 1D sequence-based model

    This is safe because there are no Lambda layers, hence no dependency on Python's bytecode.
    """
    base_path = os.path.dirname(os.path.abspath(__file__))
    model_path = "models/ss_pred_resnet_elu_nolr_dropout01_l26_large_v3_saved_model.h5"

    ss_model = load_model(os.path.join(base_path, model_path))

    inputs_seq_ = [Input(shape=(None, 22), dtype=K.floatx(), name="seq"), # sequence
                  Input(shape=(None, 23), dtype=K.floatx(), name="self_info"), # self-information
                  Input(shape=(None, 23), dtype=K.floatx(), name="part_entr")]  # partial entropy

    seq_feature_model = ss_model._layers_by_depth[5][0]
    assert 'model' in seq_feature_model.name, seq_feature_model.name
    seq_feature_model.name = 'sequence_features'

    bottleneck_seq = seq_feature_model(inputs_seq_)
    model_1D_outer = Lambda(self_outer)(bottleneck_seq)
    bottleneck_model = Model(inputs=inputs_seq_, outputs=model_1D_outer)
    return bottleneck_model, ss_model

class PolishOutputLayer(torch.nn.Module):

    def __init__(self):
        super(PolishOutputLayer,self).__init__()

    def forward(self,x):
        x = (x + x.transpose(-2,-1))/2.
        x[0,:-1] = x[0,:-1].sigmoid()
        return x

def get_tiramiprot():
    """Get the TiramiProt architecture."""
    base_path = os.path.dirname(os.path.abspath(__file__))
    model_path = os.path.join(base_path,'models/model.t7')
    hyperparam_path = os.path.join(base_path,'models/model_hyperparams.h5')

    hyperparam_dict = {}
    with h5py.File(hyperparam_path,'r') as f:
        for key in f:
            if key == 'dropout_p':
                hyperparam_dict[key] = f[key][()]
            else:
                hyperparam_dict[key] = int(f[key][()])

    base_model = Network(**hyperparam_dict)
    if torch.cuda.is_available():
        base_model.load_state_dict(torch.load(model_path))
        model = torch.nn.Sequential(base_model, PolishOutputLayer().cuda())
    else:
        base_model.load_state_dict(torch.load(model_path,map_location='cpu'))
        model = torch.nn.Sequential(base_model, PolishOutputLayer().cpu())

    return model

