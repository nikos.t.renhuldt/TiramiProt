from __future__ import unicode_literals
import os

import numpy as np
import gaussdca

from .parsing._load_data import process_a3m, load_a3m
from .parsing._mi_info import compute_mi_scores

from .create_model import create_ss_model
from keras import backend as K
import torch
from torch.autograd import Variable

def _generate_features(fname, verbose=0):
    feat_lst = ['gdca', 'cross_h', 'nmi_corr', 'mi_corr', 'seq', 'part_entr',
                'self_info']

    if not os.path.isfile(fname):
        raise IOError("Alignment file does not exist.")

    if verbose > 1:
        print('Extracting column statistics')
    self_info, part_entr, seq = process_a3m(fname)
    seq_dict = {'seq': seq, 'part_entr': part_entr, 'self_info': self_info}
    original_length = seq.shape[-2]

    if verbose > 1:
        print('Computing mutual information')
    a3m_ali = load_a3m(fname)
    mi_dict = compute_mi_scores(a3m_ali)

    if verbose > 1:
        print('Running GaussDCA')
    gdca_dict = gaussdca.run(fname)

    feat_dict = {}
    for feat in feat_lst:
        if feat == 'gdca':
            x_i = gdca_dict['gdca_corr']
            x_i = x_i[..., None]
        elif feat in ['cross_h', 'nmi_corr', 'mi_corr']:
            x_i = mi_dict[feat]
            x_i = x_i[..., None]
        elif feat in ['seq', 'part_entr', 'self_info']:
            x_i = seq_dict[feat]
        else:
            raise ValueError('Unkown feature {}'.format(feat))
        L = x_i.shape[0]
        feat_dict[feat] = x_i[None, ...]

    return feat_dict, original_length

def _convert_features_to_torch(x_dict):
    x_dict['input'] = torch.Tensor()
    for xk in [ 'gdca', 'cross_h', 'nmi_corr', 'mi_corr', 'ssmodel_output']:
        x_dict[xk] = torch.from_numpy(x_dict[xk]).squeeze()
        if xk == 'ssmodel_output':
            x_dict[xk] = x_dict[xk].permute(2,0,1)
        else:
            x_dict[xk] = x_dict[xk].unsqueeze(0)
        x_dict['input'] = torch.cat([x_dict['input'],x_dict[xk].float()],0)
    x_dict['input'] = x_dict['input'].unsqueeze(0)
    return Variable(x_dict['input'], requires_grad = False)

def _predict_contacts(model, model_input_var):
    output_var = model(model_input_var)
    output_np = output_var.data.cpu().numpy()
    for output_i in output_np[0]:
        np.fill_diagonal(output_i,0)
    return dict(cmap_6 = output_np[0,0], cmap = output_np[0,1], cmap_10 = output_np[0,2],
            s_score = output_np[0,3])

def predict(model, alignment, verbose=0):
    return predict_contacts(model, alignment, verbose)


def predict_contacts(model, alignment, verbose=0):
    feat_dict, L = _generate_features(alignment, verbose)
    bottleneck_model, _ = create_ss_model()
    feat_dict['ssmodel_output'] = bottleneck_model.predict(feat_dict)
    # Uncomment to clear memory. Seeing if this works. Would be nice.
    #K.clear_session()
    model_input_var = _convert_features_to_torch(feat_dict)
    if next(model.parameters()).is_cuda:
        model_input_var = model_input_var.cuda()
    if verbose:
        print('Features generated')
        print('Predicting')

    output_dict = _predict_contacts(model, model_input_var)
    output_dict['features'] = feat_dict
    return output_dict

