import torch.nn as nn
import torch
import numpy as np
from .TiramisuLayers import TransitionUp, center_crop, TransitionDown, _DenseBlock

""" Some of the following code has been adapted from the Fully Convolutional DenseNet.
 Original code may be found at
 https://github.com/SimJeg/FC-DenseNet
 S. Jégou, M. Drozdzal, D. Vazquez, A. Romero, and Y. Bengio, “The One Hundred Layers Tiramisu: Fully Convolutional DenseNets for Semantic Segmentation,” arXiv:1611.09326 [cs], Nov. 2016.

 Code also adapted from the Pytorch implementation of the Memory-Efficient DenseNet.
 Original code may be found at
 https://github.com/gpleiss/efficient_densenet_pytorch
 G. Pleiss, D. Chen, G. Huang, T. Li, L. van der Maaten, and K. Q. Weinberger, “Memory-Efficient Implementation of DenseNets,” arXiv:1707.06990 [cs], Jul. 2017.
"""

class Network(nn.Module):

    def __init__(self,
            n_criteria=4,
            n_filters_first_conv = 48,
            n_pool = 4,
            growth_rate = 12,
            n_layers_per_block = 5,
            dropout_p = 0.2,
            bn_size = None):
        super(Network,self).__init__()

        # Taken from Tiramisu
        if type(n_layers_per_block) is list:
            assert (len(n_layers_per_block) == 2 * n_pool + 1)
        elif type(n_layers_per_block) is int:
            n_layers_per_block = [n_layers_per_block] * (2 * n_pool + 1)
        else:
            print('n_layers_per_block must be integer or list with length equal to the total number of blocks.')
            raise ValueError


        # Taken from efficient_densenet
        # First conv
        self.downsampling_path = [nn.Sequential()]
        self.downsampling_path[-1].add_module('conv0', nn.Conv2d(132, n_filters_first_conv, kernel_size=3, stride=1, padding=1, bias=True))


        # Adapted from Tiramisu and efficient_densenet
        # Downsampling path
        skip_connection_features = [] 
        num_features = n_filters_first_conv
        for i in range(n_pool):
            # Dense block
            # Dense block returns concatenated ouputs in forward
            num_layers = n_layers_per_block[i]
            block = _DenseBlock(num_layers = num_layers,
                    num_input_features = num_features,
                    growth_rate = growth_rate,
                    dropout_p = dropout_p)
            self.downsampling_path.append(nn.Sequential())
            self.downsampling_path[-1].add_module('denseblock%d' % (i+1), block)
            num_features = num_features + num_layers * growth_rate
            skip_connection_features.append(num_features)

            trans = TransitionDown(num_input_features = num_features,
                    num_output_features = num_features,
                    dropout_p = dropout_p)
            self.downsampling_path.append(nn.Sequential())
            self.downsampling_path[-1].add_module('transitiondown%d' % (i+1), trans)
        
        skip_connection_features = skip_connection_features[::-1]
        
        self.downsampling_path = nn.ModuleList(self.downsampling_path)

        # Bottleneck
        num_layers = n_layers_per_block[n_pool]
        block = _DenseBlock(num_layers = num_layers,
                num_input_features = num_features,
                growth_rate = growth_rate,
                dropout_p = dropout_p)
        self.bottleneck = nn.Sequential()
        self.bottleneck.add_module('bottleneck', block)

        # Upsampling
        self.upsampling_path = []
        for i in range(n_pool):
            n_filters_keep = growth_rate * n_layers_per_block[n_pool + i]
            num_features = num_layers * growth_rate
            trans = TransitionUp(num_input_features = num_features,
                    num_output_features = n_filters_keep)
            self.upsampling_path.append(trans)

            num_layers = n_layers_per_block[n_pool + i + 1]
            num_features = n_filters_keep + skip_connection_features[i]
            block = _DenseBlock(num_layers = num_layers,
                    num_input_features = num_features,
                    growth_rate = growth_rate,
                    dropout_p = dropout_p)
            self.upsampling_path.append(nn.Sequential())
            self.upsampling_path[-1].add_module('denseblock%d' % (n_pool + i + 2), block)
            num_features = num_features + num_layers * growth_rate

        self.upsampling_path = nn.ModuleList(self.upsampling_path)

        finalConvLayer = nn.Conv2d(in_channels = num_features,
                out_channels = n_criteria,
                kernel_size = 1)
        self.finalConv = nn.Sequential()
        self.finalConv.add_module('finalconv',finalConvLayer)

        for m in self.modules():
            if isinstance(m,nn.Conv2d):
                nn.init.kaiming_uniform(m.weight.data)


    def forward(self,x):
        intermediate_ds_output = [x]
        n_pool = len(self.upsampling_path) //2 

        for i in range(n_pool):
            mod = self.downsampling_path[i*2] # initial conv or TD
            x = mod(x)
            mod = self.downsampling_path[i*2 + 1] # denseblock
            x = torch.cat([x,mod(x)],dim=1)
            intermediate_ds_output.append(x)

        intermediate_ds_output = intermediate_ds_output[1:][::-1]
        
        x = self.downsampling_path[-1](x)
        x = self.bottleneck(x)

        for i in range(n_pool):
            mod = self.upsampling_path[i*2] # TU
            x = torch.cat([intermediate_ds_output[i], center_crop(mod(x),intermediate_ds_output[i].shape)],dim=1)
            mod = self.upsampling_path[i*2 + 1] # denseblock
            x_pre = x
            x = mod(x)
        
        x = torch.cat([x_pre,x],dim=1)
        x = self.finalConv(x)
        return x







            



