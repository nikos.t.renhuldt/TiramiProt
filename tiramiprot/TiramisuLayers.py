import torch
import torch.nn as nn

""" Some of the following code has been adapted from the Fully Convolutional DenseNet.
 Original code may be found at
 https://github.com/SimJeg/FC-DenseNet
 S. Jégou, M. Drozdzal, D. Vazquez, A. Romero, and Y. Bengio, “The One Hundred Layers Tiramisu: Fully Convolutional DenseNets for Semantic Segmentation,” arXiv:1611.09326 [cs], Nov. 2016.

 Code also adapted from the Pytorch implementation of the Memory-Efficient DenseNet.
 Original code may be found at
 https://github.com/gpleiss/efficient_densenet_pytorch
 G. Pleiss, D. Chen, G. Huang, T. Li, L. van der Maaten, and K. Q. Weinberger, “Memory-Efficient Implementation of DenseNets,” arXiv:1707.06990 [cs], Jul. 2017.
"""

def center_crop(mat, output_size):
    n, c, h, w = mat.shape
    on, oc, oh, ow = output_size
    i = int(round((h-oh) / 2.))
    j = int(round((w-ow) / 2.))
    return mat[:,:,i:i+oh,j:j+ow]

class TransitionUp(nn.Module):
    def __init__(self, num_input_features, num_output_features):
        super(TransitionUp, self).__init__()
        self.TU = nn.ConvTranspose2d(num_input_features, num_output_features,
            kernel_size=3, stride=2)

    def forward(self, x, output_size=None):
        return self.TU.forward(x,output_size=output_size)

class TransitionDown(nn.Sequential):
    def __init__(self,num_input_features,num_output_features,dropout_p):
        super(TransitionDown, self).__init__()
        layer = BN_ReLU_Conv(num_input_features, num_output_features, kernel_size=1, dropout_p = dropout_p)
        self.add_module('DenseLayer',layer)
        self.add_module('Pool', nn.MaxPool2d(kernel_size=2,stride=2))

class BN_ReLU_Conv(nn.Sequential):
    def __init__(self,num_input_features,num_output_features,kernel_size,dropout_p):
        super(BN_ReLU_Conv,self).__init__()
        self.add_module('norm', nn.BatchNorm2d(num_input_features))
        self.add_module('relu', nn.ReLU(inplace=True))
        self.add_module('conv', nn.Conv2d(num_input_features,num_output_features, kernel_size=kernel_size, stride=1, padding=1, bias=True))
        if dropout_p != 0.:
            self.add_module('dropout', nn.Dropout2d(p=dropout_p,inplace=True))


class _DenseBlock(nn.Module):
    def __init__(self,num_layers,num_input_features,growth_rate,dropout_p):
        super(_DenseBlock,self).__init__()
        for i in range(num_layers):
            layer = BN_ReLU_Conv(num_input_features + i * growth_rate,growth_rate, kernel_size=3, dropout_p = dropout_p)
            self.add_module('DenseLayer.{}'.format(i), layer)

    def forward(self,x):
        x_size = x.shape[1]
        for mod in self.children():
            l = mod(x)
            x = torch.cat([x,l],dim=1)
        return x[:,x_size:,:,:]





