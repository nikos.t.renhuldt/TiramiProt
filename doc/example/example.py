import tiramiprot
TEST_FILE = 'T0859.fa.hhE0.a3m'
model = tiramiprot.get_tiramiprot()
pred = tiramiprot.predict(model, TEST_FILE)
with open(TEST_FILE,'r',encoding='utf-8') as f:
    f.readline()
    seq = f.readline().strip()

print(tiramiprot.utils.format_contacts_casp(pred['cmap'],seq))
