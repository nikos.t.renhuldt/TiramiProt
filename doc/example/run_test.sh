#!/bin/bash
# Uses the container to run example script, without CUDA
singularity exec ../../singularity/TiramiProt.simg python example.py
# Uses the container to run example script, with CUDA
singularity exec --nv ../../singularity/TiramiProt.simg python example.py
